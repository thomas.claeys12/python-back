# System imports

# libs imports
from fastapi import FastAPI
from pydantic import BaseModel
# from routers import CarsRouter, UserRouter, CarsTypeRouter

from app.Router.CarsRouter import router as carsRouter
from app.Router.UserRouter import router as usersRouter
from app.Router.CarsTypeRouter import router as carsTypeRouter

# local imports

app = FastAPI()

@app.get("/")
async def say_hello():
    """Ceci est une fonction qui dit bonjour oulala
    """
    return "bonjour"

app.include_router(carsRouter, tags=["cars"])
app.include_router(usersRouter, tags=["users"])
app.include_router(carsTypeRouter, tags=["carsType"])