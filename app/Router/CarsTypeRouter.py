from fastapi import APIRouter
from app.Entity.CarsTypes import CarType

router = APIRouter()

cars_types = []

@router.get("/carsTypes")
async def get_all_cars_types():
    return cars_types

@router.get("/carsTypes/{cars_types_id}")
async def get_cars_types_by_id(cars_types_id: int):
    return list(filter(lambda x: x["id"] == cars_types_id, cars_types))

@router.delete("/carsTypes/{cars_types_id}")
async def delete_cars_types_by_id(cars_types_id: int):
    filtred_list = list(filter(lambda x: x["id"] == cars_types_id, cars_types))
    if filtred_list:
        cars_types.remove(filtred_list[0])
    return filtred_list

@router.patch("/carsTypes/{cars_types_id}")
async def patch_cars_types_by_id(cars_types_id: int, car_type: CarType):
    filtred_list = list(filter(lambda x: x["id"] == cars_types_id, cars_types))
    if filtred_list:
        filtred_list[0] = car_type
    return filtred_list

@router.post("/carsTypes")
async def create_cars_types(car_type: CarType):
    cars_types.append(car_type)
    return car_type
