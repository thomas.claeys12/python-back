from fastapi import APIRouter
from bcrypt import hashpw, gensalt

from app.Entity.Hash import Hash
from app.Entity.Users import User

router = APIRouter()

hashs = []

users = [{
    "id": 1,
    "name": "thomas",
    "username": "dea",
    "email": "thomas@gmail.com",
    "tel": "06524875963",
    "newsletter": True,
    "password_hash": "azertyuioiuytrefghj",
    "is_client": True
}]

@router.get("/users", tags=["users"], response_model_exclude_unset=True)
async def get_all_users() -> list[User]:
    return users


@router.get("/users/search", tags=["users"])
async def search_users(name: str):
    return list(filter(lambda x: x["name"] == name, users))


@router.get("/users/{user_id}", tags=["users"])
async def get_user_by_id(user_id: int, name: str | None = None):
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None:
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    return filtred_list


@router.delete("/users/{user_id}", tags=["users"])
async def delete_user_by_id(user_id: int, name: str | None = None):
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None:
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    if filtred_list:
        users.remove(filtred_list[0])
    return filtred_list


@router.patch("/users/{user_id}", tags=["users"])
async def patch_user_by_id(user_id: int, user: User):
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if filtred_list:
        filtred_list[0] = user
    return filtred_list


@router.post("/users", tags=["users"])
async def create_user(user: User):
    salt = gensalt()
    user.password_hash = hashpw(user.password_hash.encode(), salt)
    users.append(user)
    hashs.append(Hash(user=user, salt=salt))
    return user
