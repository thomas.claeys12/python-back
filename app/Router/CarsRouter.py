from fastapi import APIRouter

from app.Entity.Cars import Car
from app.Router.CarsTypeRouter import cars_types

router = APIRouter()

cars = [{
    "id": 1,
    "km": 152,
    "color": "rouge",
    "price_vente": 15000,
    "price_achat": 25000,
    "carType": {
        "id": 1,
        "brand": "BMW",
        "model": "serie 1",
        "nb_porte": 5,
        "nb_place": 5
    },
    "options": [
        "bluetooth"
    ],
    "état": "neuf",
    "construction_year": 2015,
    "parking": "parking",
    "arrival_date": "15-02-2016",
    "livraison_date": "15-02-2016"
},
    {
        "id": 2,
        "km": 152,
        "color": "rouge",
        "price_vente": 15000,
        "price_achat": 25000,
        "carType": {
            "id": 1,
            "brand": "BMW",
            "model": "serie 1",
            "nb_porte": 5,
            "nb_place": 5
        },
        "options": [
            "bluetooth"
        ],
        "état": "neuf",
        "construction_year": 2015,
        "parking": "parking",
        "arrival_date": "15-02-2016",
        "livraison_date": "15-02-2016"
    }]


@router.get("/cars", tags=["cars"], response_model_exclude_unset=True)
async def get_all_cars() -> list[Car]:
    return cars


@router.get("/cars/{cars_id}", tags=["cars"])
async def get_cars_by_id(cars_id: int):
    return list(filter(lambda x: x["id"] == cars_id, cars))


@router.delete("/cars/{cars_id}", tags=["cars"])
async def delete_cars_by_id(cars_id: int):
    filtred_list = list(filter(lambda x: x["id"] == cars_id, cars))
    if filtred_list:
        cars.remove(filtred_list[0])
    return filtred_list


@router.patch("/cars/{cars_id}", tags=["cars"])
async def patch_cars_by_id(cars_id: int, car: Car):
    filtred_list = list(filter(lambda x: x["id"] == cars_id, cars))
    if filtred_list:
        filtred_list[0] = car
    return filtred_list


@router.post("/cars", tags=["cars"])
async def create_cars(car: Car):
    filtred_list = list(filter(lambda x: x["id"] == car.carType.id, cars_types))
    if filtred_list is None:
        cars_types.append(car.carType)
    else:
        car.carType = filtred_list[0]
    cars.append(car)
    return car
