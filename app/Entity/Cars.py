import enum

from pydantic import BaseModel

from app.Entity.CarsTypes import CarType
from app.Entity.Users import User


class EnumEtat(str, enum.Enum):
    neuf = "neuf"
    nickel = "nickel"
    ok = "ok"
    bof = "bof"
    ça_va = "ça va"
    déplorable = "déplorable"


class Car(BaseModel):
    id: int
    km: int
    color: str
    price_vente: int
    price_achat: int | None = None
    carType: CarType | None = None
    options: list[str] | None = None
    état: EnumEtat | None = None
    construction_year: int | None = None
    parking: str | None = None
    arrival_date: str | None = None
    livraison_date: str | None = None
    employee: User | None = None
    previous_owner: User | None = None
    new_owner: User | None = None


def __init__(self, id: int, km: int, color: str, price_vente: int, price_achat: int | None = None,
             carType: CarType | None = None, options: list[str] | None = None, état: EnumEtat | None = None,
             construction_year: int | None = None, parking: str | None = None, arrival_date: str | None = None,
             livraison_date: str | None = None, employee: User | None = None, previous_owner: User | None = None,
             new_owner: User | None = None):
    self.id = id
    self.km = km
    self.color = color
    self.price_vente = price_vente
    self.price_achat = price_achat
    self.carType = carType
    self.options = options
    self.état = état
    self.construction_year = construction_year
    self.parking = parking
    self.arrival_date = arrival_date
    self.livraison_date = livraison_date
    self.employee = employee
    self.previous_owner = previous_owner
    self.new_owner = new_owner
