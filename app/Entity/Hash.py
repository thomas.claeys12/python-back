from pydantic import BaseModel

from app.Entity.Users import User


class Hash(BaseModel):
    user: User
    salt: str


def __init__(self, user: User, salt: str):
    self.user = user
    self.salt = salt
