from pydantic import BaseModel


class CarType(BaseModel):
    id: int
    brand: str
    model: str
    nb_porte: int | None = None
    nb_place: int | None = None


def __init__(self, id: int, brand: str, model: str, nb_porte: int | None = None, nb_place: int | None = None):
    self.id = id
    self.brand = brand
    self.model = model
    self.nb_porte = nb_porte
    self.nb_place = nb_place
