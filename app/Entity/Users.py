from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str
    username: str
    email: str
    tel: str
    newsletter: bool | None = None
    password_hash: str | None = None
    is_client: bool

def __init__(self, id: int, name: str, username: str, email: str, tel: str, newsletter: bool | None = None, password_hash: str | None = None, is_client: bool = True):
    self.id = id
    self.name = name
    self.username = username
    self.email = email
    self.tel = tel
    self.newsletter = newsletter
    self.password_hash = password_hash
    self.is_client = is_client